import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training1000/auth/data/repository/supabase.dart';

Future<void> pressSendOTP(String email, Function onResponse, Function onError) async {
  try{
    await sendOTP(email);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}
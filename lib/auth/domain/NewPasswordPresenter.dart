import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training1000/auth/data/repository/supabase.dart';

Future<void> pressChangePassword(String password, Function onResponse, Function onError) async {
  try{
    await changePassword(password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}
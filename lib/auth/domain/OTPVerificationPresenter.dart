import 'dart:async';

import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training1000/auth/data/repository/supabase.dart';

import '../data/storage/seconds.dart';



void startDelayDecrement(Function(int) callback){
  Timer(
      const Duration(seconds: 1),
          () {
        if (lostSeconds != 0) {
          lostSeconds --;
        }
        callback(lostSeconds);
        startDelayDecrement(callback);
      }
  );
}

Future<void> pressVerifyOTP(String email, String code, Function onResponse, Function onError) async {
  try{
    await verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}
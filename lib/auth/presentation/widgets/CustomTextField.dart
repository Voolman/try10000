
import 'package:flutter/material.dart';

import 'colors.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String) onChanged;
  const CustomTextField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false, required this.onChanged});


  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isObscure = true;
  var colors = LightColors();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24),
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        SizedBox(height: 8),
        SizedBox(
          height: 44,
          width: double.infinity,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: '*',
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              enabledBorder: Theme.of(context).inputDecorationTheme.enabledBorder,
              focusedBorder: Theme.of(context).inputDecorationTheme.focusedBorder,
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.hint),
              contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              suffixIcon: (widget.enableObscure) ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: Image.asset('assets/eye-slash.png'),
              ) : null
            ),
          ),
        )
      ],
    );
  }
}
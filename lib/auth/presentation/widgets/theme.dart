import 'package:flutter/material.dart';
import 'package:training1000/auth/presentation/widgets/colors.dart';

var colors = LightColors();
var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: colors.text,
      fontWeight: FontWeight.w500,
      fontSize: 24
    ),
    titleMedium: TextStyle(
        color: colors.subtext,
        fontWeight: FontWeight.w500,
        fontSize: 14
    ),
    labelMedium: TextStyle(
        color: colors.textAccent,
        fontWeight: FontWeight.w700,
        fontSize: 16
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
      backgroundColor: colors.accent,
      disabledBackgroundColor: colors.disableAccent
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4) ,
      borderSide: BorderSide(color: colors.subtext, width: 1)
    ),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4) ,
        borderSide: BorderSide(color: colors.subtext, width: 1)
    )
  )
);
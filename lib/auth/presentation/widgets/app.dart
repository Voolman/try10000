import 'package:flutter/material.dart';
import 'package:training1000/auth/presentation/widgets/theme.dart';

import '../pages/SignUp.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      home: const SignUp(),
    );
  }
}
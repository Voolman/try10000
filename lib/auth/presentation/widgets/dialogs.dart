import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text('Ошибка'),
    content: Text(e),
    actions: [
      TextButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          child: const Text('OK'))
    ],
  ));
}

void showLoading(BuildContext context){
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) => PopScope(
        canPop: false,
      child: Dialog(
        surfaceTintColor: Colors.transparent,
        backgroundColor: Colors.transparent,
        child: Center(
          child: Transform.scale(
            scale: 1.5,
            child: const CircularProgressIndicator(),
          ),
        ),
      )
    )
  );
}
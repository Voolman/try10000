import 'package:flutter/material.dart';
import 'package:training1000/auth/domain/ForgotPasswordPresenter.dart';
import 'package:training1000/auth/presentation/pages/LogIn.dart';
import 'package:training1000/auth/presentation/pages/OTPVerification.dart';
import 'package:training1000/auth/presentation/widgets/CustomTextField.dart';
import 'package:training1000/auth/presentation/widgets/dialogs.dart';
import '../widgets/colors.dart';


class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});


  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController email = TextEditingController();
  void onChanged(_){}

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Восстановление пароля',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8,),
                Row(
                  children: [
                    Text(
                      'Введите свою почту',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4,),
                CustomTextField(
                    label: 'Почта',
                    hint: '***********@mail.com',
                    controller: email,
                    onChanged: onChanged
                ),

                const SizedBox(height: 503),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: () {
                      showLoading(context);
                      pressSendOTP(
                          email.text,
                          (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => OTPVerification(email: email.text)));},
                          (String e){showError(context, e);}
                      );
                    },
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Отправить код',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Я вспомнил свой пароль! ',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Вернуться',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                          )
                        ]
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
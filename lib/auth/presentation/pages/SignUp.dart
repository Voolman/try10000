
import 'package:flutter/material.dart';
import 'package:training1000/auth/domain/SignUpPresentation.dart';
import 'package:training1000/auth/presentation/pages/LogIn.dart';
import 'package:training1000/auth/presentation/widgets/CustomTextField.dart';
import 'package:training1000/auth/presentation/widgets/dialogs.dart';

import '../widgets/colors.dart';
import 'Holder.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});


  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController passwordConfirm = TextEditingController();
  void onChanged(_){}

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      body: ListView(
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Создать аккаунт',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8,),
                Row(
                  children: [
                    Text(
                      'Завершите регистрацию чтобы начать',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4,),
                CustomTextField(
                    label: 'Почта',
                    hint: '***********@mail.com',
                    controller: email,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: 'Пароль',
                    hint: '**********',
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: 'Повторите пароль',
                    hint: '**********',
                    enableObscure: true,
                    controller: passwordConfirm,
                    onChanged: onChanged
                ),
                const SizedBox(height: 319),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: () {
                      showLoading(context);
                      pressSignUp(
                          email.text,
                          password.text,
                          (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Holder()), (route) => false);},
                          (String e){showError(context, e);}
                      );
                    },
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Зарегистрироваться',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'У меня уже есть аккаунт! ',
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                        ),
                        TextSpan(
                          text: 'Войти',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                        )
                      ]
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
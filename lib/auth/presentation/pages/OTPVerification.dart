import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:training1000/auth/data/storage/seconds.dart';
import 'package:training1000/auth/domain/ForgotPasswordPresenter.dart';
import 'package:training1000/auth/domain/OTPVerificationPresenter.dart';
import 'package:training1000/auth/presentation/pages/LogIn.dart';
import 'package:training1000/auth/presentation/pages/NewPassword.dart';
import 'package:training1000/auth/presentation/widgets/dialogs.dart';
import '../widgets/colors.dart';


class OTPVerification extends StatefulWidget {
  final String email;
  const OTPVerification({super.key, required this.email});


  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {

  @override
  void initState() {
    super.initState();
    startDelayDecrement((p0) {
      setState(() {});
    });
  }

  TextEditingController code = TextEditingController();
  void onChanged(_){}

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Верификация',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8,),
                Row(
                  children: [
                    Text(
                      'Введите 6-ти значный код из письма',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 58),
                Pinput(
                  length: 6,
                  controller: code,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  defaultPinTheme: PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                      border: Border.all(color: colors.subtext, width: 1),
                      borderRadius: BorderRadius.zero
                    )
                  ),
                  submittedPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: colors.accent, width: 1),
                          borderRadius: BorderRadius.zero
                      )
                  ),
                ),
                const SizedBox(height: 48),
                (lostSeconds != 0) ?
                Text(
                    'Получить код повторно через $lostSecondsс',
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                ) :
                    GestureDetector(
                      onTap: (){
                        pressSendOTP(
                            widget.email,
                            (){
                              setState(() {
                                lostSeconds = 60;
                              });
                            },
                            (String e){showError(context, e);}
                        );
                      },
                      child: Text(
                        'Получить новый код',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                      ),
                    ),

                const SizedBox(height: 445),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: () {
                      showLoading(context);
                      pressVerifyOTP(
                          widget.email,
                          code.text,
                              (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const NewPassword()));},
                              (String e){showError(context, e);}
                      );
                    },
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Сбросить пароль',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Я вспомнил свой пароль! ',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Вернуться',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                          )
                        ]
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:training1000/auth/domain/HolderPresenter.dart';
import 'package:training1000/auth/presentation/pages/LogIn.dart';
import 'package:training1000/auth/presentation/widgets/CustomTextField.dart';

import '../widgets/colors.dart';
import '../widgets/dialogs.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});


  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.symmetric(vertical: 399, horizontal: 24),
        child:  SizedBox(
          height: 46,
          width: double.infinity,
          child: FilledButton(
            onPressed: () {
              showLoading(context);
              pressSignOut(
                      (){exit(0);},
                      (String e){showError(context, e);}
              );
            },
            style: Theme.of(context).filledButtonTheme.style,
            child: Text(
              'ВЫХОД',
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
        ),
      )
    );
  }
}